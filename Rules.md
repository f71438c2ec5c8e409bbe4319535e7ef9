# Tulpa Alcove Code of Conduct

## Our Pledge

In the interest of fostering an open and welcoming environment, we as
administrators/moderators and room members pledge to make participation in
our community a harassment-free experience for everyone, regardless of a
distinction of any kind, such as age, body size, disability, ethnicity, sex
characteristics, gender identity and expression, level of experience,
education, socio-economic status, nationality, personal appearance, race,
religion, or sexual identity and orientation.

## Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention
  or advances
* Trolling, insulting/derogatory, antagonistic comments and personal or
  political attacks
* Promoting intolerance
* Public or private harassment
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting

### Tulpas and other Mindforms

Tulpas and other mindforms are treated as equal to others. Any treatment of them being lesser is prohibited.

### Names

Please keep your display names respectful. If you won't want to change
them everywhere, use `/myroomnick` to change your name only for this room.
Moderator discretion is used to determine if your name is respectful - if
you are asked to change it, please do so.

### Advertisement of other rooms

Advertisement of other rooms out of context is not allowed. Use common sense
on what is advertisement. If, for example, another room happens to come up
organically in a conversation, or someone asks for a room about a specific
topic / where they can discuss a specific topic suggesting a room is okay,
too. When in doubt, ask a moderator!

## Our Responsibilities

Room administrators/moderators are responsible for clarifying the standards
of acceptable behavior and are expected to take appropriate and fair
corrective action in response to any instances of unacceptable behavior.

Room administrators/moderators have the right and responsibility to remove,
edit, or reject comments and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any member for other behaviors that they deem
inappropriate, threatening, offensive, or harmful.

## Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting a administrator/moderator in private.
The reports should include information on whether they can be shared to
other administrators/moderators and how much may be told.

All complaints will be reviewed and investigated and will result in a
response that is deemed necessary and appropriate to the circumstances.
The administrator/moderator team is obligated to maintain confidentiality
with regard to the reporter of an incident. Further details of specific
enforcement policies may be posted separately.

Moderators who do not follow or enforce the Code of Conduct
in good faith may face temporary or permanent repercussions as determined by
other members of the groups administrators.

## Attribution

This Code of Conduct is forked [privacytools.io Code of Conduct] from and licensed under Creative Commons
BY-4.0 (which again is forked from [Contributor Covenant version 1.4].

[privacytools.io Code of Conduct]:https://github.com/privacytoolsIO/privacytools.io/blob/master/CODE_OF_CONDUCT.md
[Contributor Covenant version 1.4]:https://contributor-covenant.org/version/1/4
